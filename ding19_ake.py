import math
from poly import *
import random
import helper
from ctypes import *
import cProfile
import time
from itertools import repeat
from multiprocessing import Pool

# https://eprint.iacr.org/2019/665.pdf
class Ding19AKE(object):
    def __init__(self, n, q, sigma):
        self.n = n
        self.q = q
        self.sigma = sigma
        self.a = Poly.uniform(self.n, self.q)
        self.si = Ding19AKE.gen_secret(self.n, self.q, self.sigma)
        self.ei = Ding19AKE.gen_pubkey_error(self.n, self.q, self.sigma)
        self.sj = Ding19AKE.gen_secret(self.n, self.q, self.sigma)
        self.ej = Ding19AKE.gen_pubkey_error(self.n, self.q, self.sigma)

    @staticmethod
    def gen_secret(n, q, sigma):
        return Poly.discretegaussian(n, q, sigma)

    @staticmethod
    def gen_pubkey_error(n, q, sigma):
        return Poly.discretegaussian(n, q, sigma)

    @staticmethod
    def gen_shared_error(n, q, sigma):
        return Poly.discretegaussian(n, q, sigma)

    @staticmethod
    def sig(q, v):
        if v > round(q / 4.0) and v < q - math.floor(q / 4):
            return 1
        else:
            return 0

    def signal(self, v):
        return [Ding19AKE.sig(self.q, v[i]) for i in range(self.n)]

    def mod2(self, v, w):
        if self.n != v.n or self.n != len(w): raise Exception
        r = [0 for i in range(self.n)]
        for i in range(self.n):
            r[i] = int(v[i] + w[i] * (self.q - 1) / 2)
            r[i] %= self.q
            r[i] %= 2
        return r
        
    def hash1(self, Pi, Pj, xi):
        seed = hash(str(Pi) + str(Pj) + str(xi))
        return Poly.discretegaussian(self.n, self.q, self.sigma, seed)
        
    def hash2(self, Pi, Pj, xi, yj):
        seed = hash(str(Pi) + str(Pj) + str(xi) + str(yj))
        return Poly.discretegaussian(self.n, self.q, self.sigma, seed)

    def alice_init(self):
        self.pi = self.a * self.si + 2 * self.ei
        self.ri = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.fi = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.xi = self.a * self.ri + 2 * self.fi
        return self.xi

    def bob(self, Pari, Parj, xi):
        self.pj =  self.a * self.sj + 2 * self.ej
        self.rj = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.fj = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.yj = self.a * self.rj + 2 * self.fj
        c = self.hash1(Pari, Parj, xi)
        d = self.hash2(Pari, Parj, xi, self.yj)
        self.gj = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.hj = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        xibar = xi + self.a * c + 2 * self.gj
        self.kj = (self.pi + xibar) * (self.sj + self.rj + d) + (-1) * self.pi * self.sj + 2 * self.hj
        self.wj = self.signal(self.kj)
        self.skj = self.mod2(self.kj, self.wj)
        return (self.yj, self.wj, self.skj, self.rj, self.gj, self.hj)
    
    def alice_resp(self, Pari, Parj, yj, wj):
        c = self.hash1(Pari, Parj, self.xi)
        d = self.hash2(Pari, Parj, self.xi, yj)
        self.gi = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        self.hi = Ding19AKE.gen_shared_error(self.n, self.q, self.sigma)
        yjbar = yj + self.a * d + 2 * self.gi
        self.ki = (self.pj + yjbar) * (self.si + self.ri + c) + (-1) * self.pj * self.si + 2 * self.hi
        self.ski = self.mod2(self.ki, wj)
        return self.ski

def get_zeros(coeffs, n):
     zero_count = 0
     temp_count = 0
     for i in range(n):
         if coeffs[i] == 0:
             temp_count += 1
         else:
             if temp_count > zero_count:
                 zero_count = temp_count
             temp_count = 0
     return zero_count

def process_k(k, n, kn_bnd, t):
     global execution
     global f
     Parj = "Bob"
     Pari = 0
     signals = [None for i in range(n)]
     pi = execution.pi
     a = execution.a
     xi = ((k+1)*t) * f
     got_signals = 0
     while got_signals < n:
         Pari = Pari + 1
         (yj,wj,skj, rj, gj, hj) = execution.bob(Pari, Parj, xi)
         pj = execution.pj
         c = execution.hash1(Pari, Parj, xi)
         d = execution.hash2(Pari, Parj, xi, yj)
         known = pi * rj + pi * d + 2 * hj + xi * rj  + xi * d + pj * c + a * c * rj + a * c * d + 2 * gj * rj + 2 * gj * d         
         for i in range(n):
             if abs(known[i]) <= kn_bnd and signals[i] == None:
                 signals[i] = wj[i]
                 got_signals += 1
     return signals


def collect_signals(n, q, istar, kn_bnd, t):
     global f
     f[0] = 1
     f[istar] = 1

     num_k = q // t

     pool = Pool(num_k)
     signals = pool.starmap(process_k, zip(range(num_k), repeat(n), repeat(kn_bnd), repeat(t)))
     pool.close()
     pool.join()
     f[istar] = 0
     return signals

def collect_abs(n, q, sigma, istar, kn_bnd, t):
     signals = collect_signals(n, q, istar, kn_bnd, t)
     coeffs_abs = list(range(n))
     for i in range(n):
         f = [signals[k][i] for k in range(q // t)]
         coeffs_abs[i] = helper.likeliest_abs_secret_from_signals_count(f, Ding19AKE.sig, sigma)
         if istar == 0 and coeffs_abs[i] != abs(execution.sj[i]):
             print("error: ", coeffs_abs[i], execution.sj[i], f)
     return coeffs_abs

def attack_step3(n, q, coeffs, MAX_ISTAR):
     sign_comp = [[None for j in range(n)] for istar in range(n)]
     for istar in range(1, MAX_ISTAR):
         for i in range(istar, n):
             if coeffs[0][i] != 0 and coeffs[0][i - istar] != 0:
                 if coeffs[istar][i] == coeffs[0][i] + coeffs[0][i - istar]: sign_comp[i][i - istar] = "S"
                 else: sign_comp[i][i - istar] = "D"
         for i in range(istar):
             if coeffs[0][i] != 0 and coeffs[0][(i - istar) % n] != 0:
                 if coeffs[istar][i] == coeffs[0][i] - coeffs[0][(i - istar) % n]:
                     sign_comp[i][(i - istar) % n] = "S"
                 else:
                     sign_comp[i][(i - istar) % n] = "D"
     coeffs_signed = Poly(n, q)
     for i in range(n): coeffs_signed[i] = coeffs[0][i]
     for i in range(0, n):
         if coeffs_signed[i] != 0:
             pos_votes = 0
             neg_votes = 0
             for j in range(0, i):
                 if coeffs_signed[j] < 0:
                     if sign_comp[i][j] == "S": neg_votes += 1
                     if sign_comp[i][j] == "D": pos_votes += 1
                 elif coeffs_signed[j] > 0:
                     if sign_comp[i][j] == "S": pos_votes += 1
                     if sign_comp[i][j] == "D": neg_votes += 1
             if neg_votes > pos_votes: coeffs_signed[i] *= -1
     return coeffs_signed

def attack(n, q, sigma, a, sj, kn_bnd, kn_bnd2, t, t2):
     coeffs_abs = collect_abs(n, q, sigma, 0, kn_bnd, t)
     print("                           ", strarray(range(n)))
     print("istar = ", 0, "coeffs[istar] = ", strarray(coeffs_abs))
     zero_count = get_zeros(coeffs_abs,n)
     MAX_ISTAR = zero_count + 2
     coeffs = list(range(MAX_ISTAR))
     coeffs[0] = coeffs_abs
     for istar in range(1,MAX_ISTAR):
         coeffs[istar] = collect_abs(n, q, sigma, istar, kn_bnd2, t2)
         print("istar = ", istar, "coeffs[istar] = ", strarray(coeffs[istar]))
     coeffs_signed = attack_step3(n, q, coeffs, MAX_ISTAR)
     print("sB            = ", strarray(sj))
     print("coeffs_signed = ", strarray(coeffs_signed))
     return sj == coeffs_signed or sj == -1 * coeffs_signed

def strarray(a):
    s = "["
    for x in a:
        if isinstance(x, bool): s += " T" if x else " F"
        elif isinstance(x, int): s += "{:2d}".format(x)
        elif isinstance(x, str): s += "{:2s}".format(x)
        elif x is None: s += "  "
        else: assert False, "Invalid type " + x
        s += " "
    s += "]"
    return s

if __name__ == "__main__":
    n = 128
    q = 2255041
    kn_bnd = 30000 
    kn_bnd2 = 20000
    t1 = 63000
    t2 = 40000
    sigma = 2.6
    print("parameters: n = {:d}, q = {:d}, sigma = {:f}".format(n, q, sigma))

    for seed in range(1):
        start = time.time()
        print("======================")
        print("seed = ", seed)
        random.seed(seed)
        a = Poly.uniform(n, q)
        global f, execution
        f = Poly(n,q)
        execution = Ding19AKE(n, q, sigma)
        execution.alice_init()
        print("     ", strarray(range(n)))
        print("sj = ", strarray(execution.sj))
        print(attack(n,q,sigma,a,execution.sj,kn_bnd,kn_bnd2,t1,t2))
        end = time.time()
        hours, rem = divmod(end-start, 3600)
        minutes, seconds = divmod(rem, 60)
        print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
