# Improved attacks against key reuse in LWE key exchange

Paper: https://eprint.iacr.org/2020/1288

To run an attack against the DXL protocol:
- optionally, set the `t` variable in `ding12.py` to the desired collection interval
- compile the C code with the command `make` and run `python3 ding12.py`

To run an attack against the DBS reusable protocol:
- set the following parameters in `ding19.py`:
	- `n`, `q`, `sigma` to the desired protocol parameters
	- `kn_bnd` and `kn_bnd2` to the desired bounds on the known value for absolute value recovery and relative sign recovery, respectively (i.e., in the paper `kn_bnd`=`h_2` and  `kn_bnd2`=`h_2'`)
	- `t_1` and `t_2` to the desired collection intervals for absolute sign recovery and relative sign recovery, respectively
- compile the C code with the command `make CFLAGS="-DVERS=19 -DN=128 -DSTDDEV=419"` where `-DN=n` and `-DSTDDEV=sigma` (without the decimal i.e., 26 for 2.6, 3197 for 3.197 and 419 for 4.19)
- run `python3 ding19.py`

To run an attack against the DBS AKE protocol:
- set the following parameters in `ding19_ake.py`:
	- `n`, `q`, `sigma`, `kn_bnd`, `kn_bnd2`,`t_1` and `t_2` as above
- compile the C code with the command `make CFLAGS="-DVERS=19 -DN=128 -DSTDDEV=419"` where `-DN=n` and `-DSTDDEV=sigma` (without the decimal i.e., 26 for 2.6, 3197 for 3.197 and 419 for 4.19)
- run `python3 ding19_ake.py`